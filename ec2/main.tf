data "aws_ami" "amazon-linux-last" {
  most_recent = true
  owners      = ["amazon"]
  name_regex  = "^amzn2-ami-hvm-.*x86_64-gp2$"
}

resource "aws_key_pair" "sshKey" {
  key_name_prefix = "mykey-tte"
  public_key      = file(var.public_key)
}

resource "aws_instance" "myInstance" {
  ami                     = data.aws_ami.amazon-linux-last.id
  instance_type           = "t2.micro"
  key_name                = aws_key_pair.sshKey.key_name

  tags = {
    OwnerEmail  = "me@here.this"
    Name        = "myVm"
  }

  volume_tags = {
    OwnerEmail  = "me@here.this"
  }

  user_data = <<-EOF
              #! /bin/bash -xe
              sudo yum install -y httpd
              sudo systemctl start httpd
              sudo systemctl enable httpd
              EOF
}
