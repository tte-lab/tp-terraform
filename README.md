# Tuto prise en main de terraform 

## 1. Concept general 
### 1.1 hcl
HCL (HashiCorp Configuration Language) est un langage de configuration open-source développé par HashiCorp. Il est utilisé pour décrire les infrastructures informatiques de manière déclarative, c'est-à-dire en spécifiant ce que l'on souhaite obtenir plutôt que comment l'obtenir. HCL est utilisé dans plusieurs outils de HashiCorp, tels que Terraform, Packer et Vault. Il permet de décrire les ressources d'infrastructure, les paramètres et les variables de manière claire et simple.
### 1.2 provider / tf / tfvars / tfstate

un exemple de fichier terraform avec les concept de basse : 
``` example.tf
provider "aws" {
  region = "us-west-2"
}

resource "aws_instance" "example" {
  ami           = "ami-0ff8a91507f77f867"
  instance_type = var.instance_type
  
  vpc_security_group_ids = ["sg-01234567890abcdef0"]
  tags = {
    Name = "example-instance"
  }
  
  associate_public_ip_address = true
  key_name = var.key_name
}

variable "instance_type" {
  default = "t2.micro"
}

variable "key_name" {
  default = "my_key"
}

data "aws_availability_zones" "all" {}

output "availability_zones" {
  value = data.aws_availability_zones.all.names
}
```

provider : spécifie le(s) provider(s) a charger pour pouvoir utiliser ses ressources. 


resource : un objet cloud dans l'example :

    ==> resource "aws_instance" "example"  :
    -> aws_instance : type de resource fournie par les provider 
    -> nom donné à la ressource dans le scope de terraform

variable : définie une variable d'entrée du code teraform, peut avoir un type une description et une valeure par défaut

data : inversement à la ressource qui créer une entité sur le provider le data permet de recupérer une entité existante dans le provider pour utiliser ses attributs

output : fournis une valeur en sortie du code terraform 


## 2. Déploiement simple d'une 
cf demo 
## 3. Backend et tfstate 
cf demo 
## 4. Modules
Voici un exemple de structure de fichiers pour un module Terraform :

````
module-name/
├── main.tf
├── variables.tf
├── outputs.tf
├── README.md
└── examples/
    └── example.tf
````

- main.tf contient la logique de provisionnement des ressources.
- variables.tf contient les variables du module.
- outputs.tf contient les sorties du module.
- README.md contient la documentation du module.
- examples/ contient des exemples d'utilisation du module.


Voici un exemple de contenu pour chacun des fichiers :


```main.tf
resource "aws_instance" "example" {
  ami           = var.ami
  instance_type = var.instance_type
}
```

```variable.tf
variable "ami" {
  type = string
  default = "ami-0ff8a91507f77f867"
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}
```



```output.tf
output "example_ip" {
  value = aws_instance.example.public_ip
}
```

```README
# Module Name

This module creates an EC2 instance in AWS.

## Inputs

- `ami`: The AMI ID to use for the EC2 instance.
- `instance_type`: The type of instance to launch.

## Outputs

- `example_ip`: The public IP address of the created EC2 instance.
```


```Example
module "example" {
  source = "module-name"
  ami = "ami-0ff8a91507f77f867"
  instance_type = "t2.micro"
}
```
